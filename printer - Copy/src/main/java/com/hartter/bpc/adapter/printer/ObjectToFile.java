package com.hartter.bpc.adapter.printer;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.xml.sax.SAXException;

import com.hartter.bpc.adapter.printer.Restrepository.Restservice;

import io.swagger.parser.SwaggerParser;
import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Swagger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import io.swagger.parser.SwaggerParser;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ByteArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.Response;
//import io.swagger.models.Path;
import io.swagger.models.Model;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.refs.RefType;


import java.io.Serializable;  
import java.util.Date;

public class ObjectToFile {
	
	public static  void createFunction(String restMethod1, Restrepository.Restservice restservice1,io.swagger.models.Path sPath1,Swagger swagger1){
		System.out.println("function createFunction start ----");
		Restrepository.Restservice.Function function = ObjectFactory.createRestrepositoryRestserviceFunction();
    	Restrepository.Restservice.Function.Restcall restcall = ObjectFactory.createRestrepositoryRestserviceFunctionRestcall();
    	Restrepository.Restservice.Function.Restcall.Httpinfo httpinfo = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfo();
    	Restrepository.Restservice.Function.Restcall.Httpinfo.Path hPath = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoPath();
    	Restrepository.Restservice.Function.Restcall.Httpinfo.Query hQuery = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoQuery();
    	Restrepository.Restservice.Function.Restcall.Httpinfo.Body hBody = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoBody();

    	List<Parameter>lp = null;
    	switch(restMethod1){
    	case "GET": lp =sPath1.getGet().getParameters();
    				break;
    	case "POST": lp = sPath1.getPost().getParameters();
    				
    				break;
    	case "PUT":  lp = sPath1.getPut().getParameters();
    				break;
    	case "DELETE": lp = sPath1.getDelete().getParameters();
    	            break;
    	}
    	
   	 for (int i =0; i<lp.size(); i++){
   		//restrepository/restservice/function/restcall/httpinfo/path,query/parameter(s) <-path.getGet.getParameters.parameter 5(6)th layer
   		 Parameter p = lp.get(i);
   		 if (p.getIn()=="path"){
   			 Restrepository.Restservice.Function.Restcall.Httpinfo.Path.Param pParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoPathParam();
   			 pParam.setRestname(p.getName());
   			 pParam.setEainame(" ");
   			 hPath.setParam(pParam);
   		 }else if(p.getIn()=="query"){
   			 Restrepository.Restservice.Function.Restcall.Httpinfo.Query.Param qParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoQueryParam();
   			 qParam.setRestname(p.getName());
   			 qParam.setEainame(" ");
   			 hQuery.getParam().add(qParam);
   		 }else if (p.getIn()=="body"){
   			 
   			 System.out.println("-----below there will be lines for body");
   			 //to DO :
   			 //build Restrepository.Restservice.Function.Restcall.Httpinfo.Path
   			 //build Restrepository.Restservice.Function.Restcall.Httpinfo.Path.Param
   			 Restrepository.Restservice.Function.Restcall.Httpinfo.Body.Param bParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoBodyParam();
   			 String pGetIN = p.getIn();
   			
   			 bParam.setEainame("Body EAI");
   			 bParam.setRestname(p.getName());
   			 
   			 hBody.getParam().add(bParam);
   			 System.out.println("-----above there will be lines for body");
   		 }
   	 }
   	
   	//restrepository/restservice/function/restcall/httpinfo/header  fixed content 5th layer
   	Restrepository.Restservice.Function.Restcall.Httpinfo.Header hHeader = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoHeader();
   	Restrepository.Restservice.Function.Restcall.Httpinfo.Header.Param hParam = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallHttpinfoHeaderParam();
   	hParam.setRestname("Accept");
   	hParam.setEainame("httpheader.accept");
   	hHeader.getParam().add(hParam);
   	hParam.setRestname("Authorization");
   	hParam.setEainame("httpheader.authorization");
   	hHeader.getParam().add(hParam);
   	  	
   	httpinfo.setPath(hPath);
   	httpinfo.setQuery(hQuery);
   	httpinfo.setBody(hBody);
   	httpinfo.setHeader(hHeader);
   	restcall.setHttpinfo(httpinfo);
   	
   	//restrepository/restservice/function/restcall/request  fixed content  4th layer 
	    Restrepository.Restservice.Function.Restcall.Request rRequest = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallRequest();
	    rRequest.setContenttyp("application/json/utf8");
	    restcall.setRequest(rRequest);
	    
	    //restrepository/restservice/function/restcall/response  fixed content  4th layer 
	    Restrepository.Restservice.Function.Restcall.Response rResponse = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponse();
	    rResponse.setContenttyp("application/json/utf8");
	    	    	    
	    //insert context to restrepository/restservice/function/restcall/response based on status"200"
	    Map<String,Response> lr = sPath1.getGet().getResponses();//TO DO : adapt getGet to all  the methods
	    Response r = lr.get("200");
	    RefProperty rPro =(RefProperty) r.getSchema();             	    
	    String simpleRef = rPro.getSimpleRef();//depositAccounts
	    //String ref = rPro.get$ref();//#/definitions/depositAccounts//Suppose all the second part are under "definitions"
	   
	    //restrepository/restservice/function/restcall/response/json  empty attribute  5th layer
	    Restrepository.Restservice.Function.Restcall.Response.Json rJson = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJson();
	    
		Model model = swagger1.getDefinitions().get(simpleRef);
	    Map<String, Property> mp = (Map<String, Property>)model.getProperties();
	    for (String mpKey : mp.keySet() ){
	    	//restrepository/restservice/function/restcall/response/json/json:param  empty   6th layer
	    	Restrepository.Restservice.Function.Restcall.Response.Json.Par par = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonPar();
	    	par.setJsonname(mpKey);
	    //	System.out.println("mpKey --> " + mpKey);
	    //	System.out.println("mp.get(mpKey).getClass() --> " + mp.get(mpKey).getClass());
	    //	System.out.println("ObjectProperty.class -->" + ObjectProperty.class);
	    	if ( mp.get(mpKey).getClass().equals(ArrayProperty.class)){
	    		ArrayProperty oPP = (ArrayProperty)mp.get(mpKey);
					if (oPP.getItems().getClass().equals(ObjectProperty.class)) {
						ObjectProperty oPP1 = (ObjectProperty)oPP.getItems();
						
						Map<String, Property> mJP = (Map<String, Property>) oPP1.getProperties();
						for (String jpKey : mJP.keySet()){
						Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
						para.setEainame("  ");
						/*ObjectProperty oPP11 = (ObjectProperty)mJP.get(jpKey);
						Map<String, Property> mOPP = (Map<String, Property>)oPP1.getProperties();
						for (String mOPPKey : mOPP.keySet()) {
							Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param1 param1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam1();
							param1.setEainame("  ");
							param1.setJsonname(mOPPKey);
						}*/
						para.setJsonname(jpKey);
						par.getPara().add(para);                 	    		                 	    		
						}
						
					//	System.out.println("We get the para !!!!!");
					}
					//line 411 of Transfer Funds_v1.0.1.yaml
					if (oPP.getItems().getClass().equals(RefProperty.class)){
						//System.out.println("we got ref project");
					
						RefProperty rPro1 = (RefProperty) oPP.getItems();
						String simpleRef1 = rPro1.getSimpleRef();
					//	System.out.println("SimpleRef1 ---> " + simpleRef1);
						
						Model mode2 = swagger1.getDefinitions().get(simpleRef1);
					    Map<String, Property> mp1 = (Map<String, Property>)mode2.getProperties();
					    for (String mpKey1 : mp1.keySet() ){
					    	Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
							para.setEainame("  ");
							para.setJsonname(mpKey1);
							
							//add the last one Param1
							
							//ByteArrayProperty oPP11 = (ByteArrayProperty)mp1.get(mpKey1);
							//System.out.println("mp1.get(mpKey1).getClass() --> " + mp1.get(mpKey1).getClass());
							if (mp1.get(mpKey1).getClass().equals(ObjectProperty.class)){
								ObjectProperty oPP11 = (ObjectProperty)mp1.get(mpKey1);
								Map<String, Property> mOPP = (Map<String, Property>)oPP11.getProperties();
			    	    		for (String mOPPKey : mOPP.keySet()) {
			    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param1 param1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam1();
			    	    			param1.setEainame("  ");
			    	    			param1.setJsonname(mOPPKey);
			    	    			para.getParam1().add(param1);
			    	    		}
							}
							par.getPara().add(para);   
					    	
					    }
					//    System.out.println("We get the para for RefProperty !!!!!");
					    
					    
					}
	    		
	    	}
	    	
	    	if ( mp.get(mpKey).getClass().equals(ObjectProperty.class)){
	    		ObjectProperty oPP = (ObjectProperty)mp.get(mpKey);
	    		Map<String, Property> mJP = (Map<String, Property>) oPP.getProperties();
	    		for (String jpKey : mJP.keySet()){
   	    		Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para para = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParPara();
    	    	para.setEainame("  ");
    	    		/*ObjectProperty oPP11 = (ObjectProperty)mJP.get(jpKey);
    	    		Map<String, Property> mOPP = (Map<String, Property>)oPP1.getProperties();
    	    		for (String mOPPKey : mOPP.keySet()) {
    	    			Restrepository.Restservice.Function.Restcall.Response.Json.Par.Para.Param1 param1 = ObjectFactory.createRestrepositoryRestserviceFunctionRestcallResponseJsonParParaParam1();
    	    			param1.setEainame("  ");
    	    			param1.setJsonname(mOPPKey);
    	    		}*/
    	    	para.setJsonname(jpKey);
    	    	par.getPara().add(para);                 	    		                 	    		
    	        }
	    		
	    		System.out.println("We get the para !!!!!");
	    	}
	    	
	    	
	    	
	    	             	    	             	    	
	    	if (!mpKey.equals("alertInfo")&&!mp.get(mpKey).getClass().equals(RefProperty.class)){
	    		rJson.getPar().add(par);
	    	}
	    		    	
	    }
   	
	    rResponse.setJson(rJson);
	    restcall.setResponse(rResponse);
	    
	    restcall.setPath("  ");
	    restcall.setMethod(restMethod1);
   	
	    function.setName("  ");
	    	    
	    function.setRestcall(restcall);
	    restservice1.getFunction().add(function);	
        
	    System.out.println("function createFunction finished ----");
	}
			
    public static void main(String[] args) throws JAXBException, IOException, SAXException {

    	//reading YAML file and creating the object holding YAML data
		final SwaggerParser parser = new SwaggerParser();
        final Swagger swagger = parser.read("Transfer Funds_v1.0.1.yaml");
    	   	
    	//Createing object for xml and binding data to this object
    	// Create Context
        final JAXBContext context = JAXBContext.newInstance(Restrepository.class);
        // Java to XML -> Marshaller needed
        final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);        
               
        // Using ObjectFactory and creating the object for xml
        ObjectFactory objectFactory = new ObjectFactory();
        
        //create restrepository 1st layer
        Restrepository restrepository = objectFactory.createRestrepository();
        
        restrepository.setxmlns("http://hartter.com/vsop/eai/eairestservice/repository");
        restrepository.setxmlnsjson("http://hartter.com/vsop/eai/eaiserviceutil/basejson/repository");
        
        //create restrepository/restservice <-- paths 2nd layer               
        Map<String, io.swagger.models.Path> mm = (Map<String, io.swagger.models.Path>)swagger.getPaths();
        for (String key : mm.keySet()){
        	   
        	    Restrepository.Restservice restservice = objectFactory.createRestrepositoryRestservice();
        		String keyPath = key;
        		String[] nameParts = keyPath.split("/");
        		String rsName = nameParts[1];
        		String rsPath = "/" + nameParts[1] + "/" + nameParts[2];
        		
        		restservice.setName(rsName);
            	restservice.setPath(rsPath);
            	
            	//create restrepository/restservice/function/restcall <--path 3rd layer
            	//REST METHOD is from GET, POST, PUT,DELETE, 
        	    io.swagger.models.Path sPath = mm.get(key);
        	    System.out.println("Key is --> start: " + key);
        	    System.out.println("sPath.getOperationMap()--> " + sPath.getOperationMap());
        	    System.out.println("sPath.getOperations() --> " + sPath.getOperations());
        	    System.out.println("sPath.getOptions() --> " + sPath.getOptions());
        	    System.out.println("Key is --> end: " + key);
        	    
        	    Map<HttpMethod,Operation> httpMap = (Map<HttpMethod,Operation>) sPath.getOperationMap();
        	    for (HttpMethod httpMethod : httpMap.keySet()) {
        	    	String restMethod = httpMethod.name();
        	    	createFunction(restMethod,restservice,sPath,swagger);  
        	    }
        	   // System.out.println("sPath.getOperationMap() --->"  + sPath.getOperationMap());
        	 /*   if (!sPath.getGet().equals(null)){
        	    	//Operation op = sPath.getGet();
        	    	System.out.println("We capture Rest Method GET -------");
        	    	String restMethod = "GET";  	
        	    	createFunction(restMethod,restservice,sPath,swagger);        	            	              	         	    	
        	    }*/
 /*       	    if(!sPath.getPost().equals(null)){
        	    	String restMethod = "POST";
        	    	System.out.println("We capture Rest Method POST -------");
        	    	createFunction(restMethod,restservice,sPath,swagger);
        	    	System.out.println("We capture Rest Method POST -------finished");
        	    }*/
 /*       	    if (!sPath.getPut().equals(null)){
        	    	String restMethod = "PUT";
        	    	System.out.println("We capture Rest Method PUT -------");
        	    	//createFunction(restMethod, restservice,sPath,swagger);
        	    }
        	    if (!sPath.getDelete().equals(null)){
        	    	String restMethod = "DELETE";
        	    	System.out.println("We capture Rest Method DELETE -------");
        	    	//createFunction(restMethod,restservice,sPath,swagger);
        	    }*/
        	    
        		restrepository.getRestservice().add(restservice);
        }
        
        // Marshalling
        final Path target = Paths.get("Transfer Funds_v1.0.1.new.xml");
        try (final OutputStream out = Files.newOutputStream(target)) {
            marshaller.marshal(restrepository, out);
        }
            
    }
}