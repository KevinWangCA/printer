package com.hartter.bpc.adapter.printer;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import io.swagger.parser.SwaggerParser;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.Path;
import io.swagger.models.Model;

import java.io.Serializable;  
import java.util.Date;  
  
import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;  

import test.XMLUtil;

@XmlRootElement(name = "YamlFileObject") 

public class Printer {

	/**
	 * @param args
	 */
	/*public String file;
	public SwaggerParser parser;
	public YamlFileObject yamlFileObject;
	

	public void insertYamlFileObject(String file){
		yamlFileObject.setHost(parser.read(file).getHost());
		
	}*/
		
	public  static void main(String[] args) {
		
		
		final SwaggerParser parser = new SwaggerParser();
        final Swagger swagger = parser.read("GetDepositAccountDDA_v1.0.0.yaml");
        
  //  get general information and set to object      
       /* String pa = swagger.getPaths().keySet().toString();
        pa.replaceAll("]","");
        System.out.println("getPathString: ->" + pa);
        System.out.println("getPathsKeySet: ->" + swagger.getPaths().keySet());
      //  System.out.println("getPaths: -> " + swagger.getPaths());
        System.out.println("getHost: ->" + swagger.getHost());
        //System.out.println("getProduces: ->" + swagger.getProduces().get(0));
       // System.out.println("getSchemess: ->" + swagger.getSchemes().get(0));
*/        
   
  //   get general information and set to object (new version)    
        YamlFileObject yamlFileObject = new YamlFileObject ();
        
        System.out.println("getHost: ->" + swagger.getHost());
        yamlFileObject.setHost(swagger.getHost());		
        System.out.println("Host of yamlFileObject is: " + yamlFileObject.getHost());
        
        System.out.println("getSchemes: ->" + swagger.getSchemes().get(0).toValue());
        yamlFileObject.setSchemes(swagger.getSchemes().get(0).toValue());		
        System.out.println("Schemes of yamlFileObject is: " + yamlFileObject.getSchemes());
        
       
        
        
   // get parameters and set to object
       /* ArrayList<Parameter> ap = (ArrayList<Parameter>)swagger.getPaths().get("/CeleritiDepositsAPI/services/v1/depositAccountsDda/{accountNbr}").getGet().getParameters(); 
        System.out.println(ap.size());
         
        ArrayList<YamlFileObjectParameter> yamlFileObjectParameters = new ArrayList<YamlFileObjectParameter>();
   
        int s = ap.size();
        
        for (int i =0; i<s; i++){
        	
        	Parameter p = ap.get(i);
        	//System.out.println(p.getDescription());
        	//System.out.println(p.getName());
        	//System.out.println(p.getIn());
        	YamlFileObjectParameter yfop = new YamlFileObjectParameter();
        	yfop.setIn(p.getIn());
        	System.out.println("Variant In of Object Parameters No."+ i +" is " + yfop.getIn());
        	yfop.setName(p.getName());
        	System.out.println("Variant Name of Object Parameters No." + i +" is " + yfop.getName());
         	yamlFileObjectParameters.add(i, yfop);
         	System.out.println("yamlFileObjectParameters No. In "+ i + " is " + yamlFileObjectParameters.get(i).getIn());
        	
        }
        
        yamlFileObject.setParameters(yamlFileObjectParameters);*/
        
        
    //get definitions and set to object
        
//       /* Map<String, Model> mm = (Map<String, Model>)swagger.getDefinitions(); 
        String properties = swagger.getDefinitions().get("depositAccounts").getProperties().toString();
        System.out.println ("properties is  " + properties);
        System.out.println (swagger.getDefinitions());
        System.out.println (mm.toString());
        
        String reference = swagger.getDefinitions().get("depositAccounts").getReference();
        System.out.println ("reference is  " + reference);
        
        int s = swagger.getDefinitions().get("depositAccounts").getProperties().size();
        System.out.println("getProperties().size " + s);
        
        Map<String, Property> mp = (Map<String, Property>)swagger.getDefinitions().get("depositAccounts").getProperties();
        System.out.println("mp.size = " + mp.size());
        System.out.println("mp.values = " +  mp.values());
        
        Collection <Property> p = (Collection <Property>)swagger.getDefinitions().get("depositAccounts").getProperties().values();
        System.out.println("getProperties().keySet() -->" + swagger.getDefinitions().get("depositAccounts").getProperties().keySet());
        System.out.println(p.toString());
        //System.out.println("getProperties to String"+ swagger.getDefinitions().get("depositAccounts").getProperties().toString());
        
        ObjectProperty p0 = (ObjectProperty) swagger.getDefinitions().get("depositAccounts").getProperties().get("standardAccountInfo");
        System.out.println("p0 get name -->" + p0.getType());
        System.out.println("p0 get vendorExtension -->" + p0.getVendorExtensions().toString());
        System.out.println("p0 get description -->" + p0.getDescription());
        p0.getProperties();
      
        Map<String, Property> deeppro = (Map<String, Property>)p0.getProperties();
        System.out.println("deeppro.getProperties().keySet() -->" + deeppro.keySet());
        System.out.println("deeppro.toString() -->" + deeppro.toString() );*/
        
//test to convert object: swagger to XML       
        String path = "KevinYamlFileObjectSimple.xml";
		System.out.println("---将对象转换成File类型的xml Start---");  
        XMLUtil.convertToXml(yamlFileObject, path);  
        System.out.println("---将对象转换成File类型的xml End---");  
        
	}

}
