package com.hartter.bpc.adapter.printer;

import java.util.ArrayList;


import javax.xml.bind.annotation.XmlAccessType;  
import javax.xml.bind.annotation.XmlAccessorType;  
import javax.xml.bind.annotation.XmlRootElement;  
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "YamlFileObject") 

public class YamlFileObject {

	private String host;   //srvpocanz001.csc-fsg.com
	private String schemes; //https
/*	private String produces; //application/json
	private ArrayList<YFOPath>yFOpaths;*/

	public YamlFileObject(){
		
	}

	public YamlFileObject(String host, String schemes) {
		super();
		this.host = host;
		this.schemes = schemes;
	/*	this.produces = produces;
		this.yFOpaths = yFOpaths;*/
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getSchemes() {
		return schemes;
	}

	public void setSchemes(String schemes) {
		this.schemes = schemes;
	}

	/*public String getProduces() {
		return produces;
	}

	public void setProduces(String produces) {
		this.produces = produces;
	}

	public ArrayList<YFOPath> getyFOpaths() {
		return yFOpaths;
	}

	public void setyFOpaths(ArrayList<YFOPath> yFOpaths) {
		this.yFOpaths = yFOpaths;
	}*/
	
	
	
}
