package com.hartter.bpc.adapter.printer;

import java.util.ArrayList;

public class YFOPath {
	private String path;
	private ArrayList<YFOMethod>yFOMethods;
	
	public YFOPath(){
		
	}

	public YFOPath(String path, ArrayList<YFOMethod> yFOMethods) {
		super();
		this.path = path;
		this.yFOMethods = yFOMethods;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ArrayList<YFOMethod> getyFOMethods() {
		return yFOMethods;
	}

	public void setyFOMethods(ArrayList<YFOMethod> yFOMethods) {
		this.yFOMethods = yFOMethods;
	}
	
	
}