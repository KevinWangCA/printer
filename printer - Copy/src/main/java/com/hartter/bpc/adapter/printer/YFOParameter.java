package com.hartter.bpc.adapter.printer;

public class YFOParameter {
	
	private String name;
	private String in;
	private String description;
	private String schema;
	private String shortSchema;
	
	public YFOParameter(){
		
	}

	public YFOParameter(String name, String in, String description,
			String schema, String shortSchema) {
		
		this.name = name;
		this.in = in;
		this.description = description;
		this.schema = schema;
		this.shortSchema = shortSchema;
	}
	
	
	
}
