package com.hartter.bpc.adapter.printer;

import java.util.ArrayList;

public class YFOMethod {
	private String tags;
	private String summary;
	private String description;
	private String operationId;
	private ArrayList<YFOParameter> yFOParameters;
	private ArrayList<YFOResponse> yFOResponses;
	
	public YFOMethod(){
		
	}

	public YFOMethod(String tags, String summary, String description,
			String operationId, ArrayList<YFOParameter> yFOParameters,
			ArrayList<YFOResponse> yFOResponses) {
		
		this.tags = tags;
		this.summary = summary;
		this.description = description;
		this.operationId = operationId;
		this.yFOParameters = yFOParameters;
		this.yFOResponses = yFOResponses;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public ArrayList<YFOParameter> getyFOParameters() {
		return yFOParameters;
	}

	public void setyFOParameters(ArrayList<YFOParameter> yFOParameters) {
		this.yFOParameters = yFOParameters;
	}

	public ArrayList<YFOResponse> getyFOResponses() {
		return yFOResponses;
	}

	public void setyFOResponses(ArrayList<YFOResponse> yFOResponses) {
		this.yFOResponses = yFOResponses;
	}

	
	
	

}
