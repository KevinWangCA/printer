package com.hartter.bpc.adapter.printer;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import io.swagger.parser.SwaggerParser;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.Path;
import io.swagger.models.Model;

public class YAMLPrinter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final SwaggerParser parser = new SwaggerParser();
        final Swagger swagger = parser.read("GetDepositAccountDDA_v1.0.0.yaml");
        
        System.out.println(swagger.getHost());
        System.out.println(swagger.getSchemes().get(0).toValue());
        System.out.println(swagger.getProduces().get(0));
        
	}

}
