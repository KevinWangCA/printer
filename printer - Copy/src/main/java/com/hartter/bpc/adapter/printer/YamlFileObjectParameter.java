package com.hartter.bpc.adapter.printer;

//import io.swagger.models.parameters.PathParameter;


public class YamlFileObjectParameter {
 
	private String name;
	private String in;
	
	public YamlFileObjectParameter(){
		
	}
	
	public YamlFileObjectParameter(String name, String in) {		
		this.name = name;
		this.in = in;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIn() {
		return in;
	}
	public void setIn(String in) {
		this.in = in;
	}
	
	
	
}
