package com.hartter.bpc.adapter.printer;

public class YFOResponse {
	
	private String status;
	private String description;
	private String schema;
	private String shortSchema;//get the last pattern of schema
	
	public YFOResponse(){
		
	}

	public YFOResponse(String status, String description, String schema,
			String shortSchema) {
		
		this.status = status;
		this.description = description;
		this.schema = schema;
		this.shortSchema = shortSchema;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getShortSchema() {
		return shortSchema;
	}

	public void setShortSchema(String shortSchema) {
		this.shortSchema = shortSchema;
	}
	
	
	
	
	
	
	
	
}
